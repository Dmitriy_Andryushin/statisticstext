import java.io.*;

public class Main {
    public static void main(String[] args) {
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("C:\\Users\\DMHLDP\\IdeaProjects\\StatisticsText\\src\\file.txt"))) {

            String strLine;
            int sumSpace = 0, sumSymbols = 0, sumWord = 0;

            while ((strLine = bufferedReader.readLine()) != null) {
                System.out.println("\n" + strLine);

                sumWord += countingOfWord(strLine);
                sumSymbols += countingOfSymbols(strLine);
                sumSpace += countingOfSpace(strLine);
            }
            System.out.printf("\nКол-во слов: %d \nКол-во символов: %d \nКол-во пробелов: %d \nКол-во символов без пробелов: %d ", sumWord, sumSymbols, sumSpace, sumSymbols - sumSpace);
        } catch (IOException ex) {
            System.out.println(ex.getMessage());
        }
    }

    private static int countingOfWord(String str) {
        str = str.replaceAll("[^-aA-zZаА-яЯ ]", " ");
        str = str.replaceAll("-", "");
        str = str.replaceAll(" {2,}", " ");
        System.out.println("В строке содержится: " + str.split(" ").length + " слов(а/о)");
        return str.split(" ").length;
    }

    private static int countingOfSymbols(String str) {
        char[] strArray = str.toCharArray();
        System.out.println("В строке содержится: " + strArray.length + " символ(а/ов).");
        return strArray.length;
    }

    private static int countingOfSpace(String str) {
        int sum = str.replaceAll("[^ ]", "").length();
        System.out.println("В строке содержится: " + sum + " пробел(а/ов).");
        return sum;
    }

}